import React from 'react';


class CounterApp extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            count:0
        }
        this.increment = this.increment.bind(this)
        this.decrement = this.decrement.bind(this)

    }

    // increment the count
    increment() {
        this.setState(prevState => {
            return {
                count: prevState.count + 1
            }
        })
    }

    // decrement the count
    decrement(){
        this.setState(prevState => {
            console.log("[revious count",prevState.count)
            if (prevState.count > 0){
            return {
                count: prevState.count - 1
            }}
        })
    }

    render(){
        //css
        const mystyle = {
            margin:"500px",
            padding:'20px'

        }

        const add = {
            backgroundColor:'blue',
            color:"white",
            marginRight:"20px",
            padding:'10px'


        }
        const remove = {
            backgroundColor:'red',
            color:"white",
            padding:'10px'
        }
        return <div style={mystyle}>
            <h2>Count:{this.state.count}</h2>
            <button style={add} onClick={this.increment}>Add</button>
            <button style={remove} onClick={this.decrement}>remove</button>
        </div>
    }
}
export default CounterApp;